package com.qa.src.tests;


import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class GetCallBDD {

    @Test
    public void test_charities(){
       Response rs = RestAssured.given().
                when().
                get("https://api.trademe.co.nz/v1/Charities.json");
       Assert.assertTrue(rs.jsonPath().getList("Description").contains("St John"));

    }


}
